# locust-plugin-git

_Git client for load testing._

## License

SPDX-License-Identifier: [MIT](https://spdx.org/licenses/MIT)

## Reference

- [Extending Locust](https://github.com/locustio/locust/blob/master/docs/extending-locust.rst)
