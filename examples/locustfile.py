from locust import task, TaskSet
from locust_plugin_git import GitUser


class ExampleUser(GitUser):
    host = "gitlab.com"
    transport_protocol = "https"
    repository_url = "infinite-omicron/eng/locust-plugin-git"

    @task
    class ExampleTasks(TaskSet):
        @task
        def clone(self):
            self.client.clone("infinite-omicron/eng/locust-plugin-git")

        @task
        def ls_remotes(self):
            self.client.ls_remotes()
