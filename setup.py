#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(
    name="locust-plugin-git",
    version="0.0.1",
    description="Git client for load testing",
    author="Lucas Ramage",
    url="https://gitlab.com/infinite-omicron/eng/locust-plugin-git",
    license="MIT",
    packages=find_packages(exclude=["examples"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=["locust"],
)
