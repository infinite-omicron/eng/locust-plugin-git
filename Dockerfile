FROM locustio/locust

USER root

COPY . /usr/src/locust_plugin_git
WORKDIR /usr/src/locust_plugin_git

RUN pip install -r requirements.txt && \
    python setup.py install

USER locust

