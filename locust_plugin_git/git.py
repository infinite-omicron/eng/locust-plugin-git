"""locust-plugin-git"""
import os
import shutil

from pygit2 import clone_repository, discover_repository, Repository
from locust import User


class GitUser(User):
    """
    Represents a git "user" which is to be spawned and attack the system that is to be load tested.
    """

    abstract = True

    host = None
    transport_protocol = None
    repository_url = None
    debug = False

    def __init__(self, environment):
        super().__init__(environment)
        self.client: GitClient = GitClient(
            environment=self.environment,
            host=self.host,
            transport_protocol=self.transport_protocol,
            repository_url=self.repository_url,
            debug=self.debug,
        )
        self.client.clone()


class GitClient:
    """
    TODO
    """

    def __init__(self, *, environment, host, transport_protocol, repository_url, debug):
        """
        TODO
        """

        self.environment = environment
        self.host = host
        self.transport_protocol = transport_protocol
        self.repository_url = repository_url
        self.debug = debug
        self.repository = None

    def _generate_full_url(self, url):
        """
        TODO
        """

        return self.transport_protocol + "://" + self.host + "/" + url

    def _generate_path(self, url):
        """
        TODO
        """

        _url_split = url.split("/")
        _path = _url_split[len(_url_split) - 1]
        return _path

    def _check_path(self, path):
        """
        TODO
        """

        _existing_path = os.getcwd() + path
        return discover_repository(_existing_path)

    def clone(self, url=""):
        """
        TODO
        """

        if not url:
            _repository_url = self._generate_full_url(self.repository_url)
            _repository_path = self._generate_path(self.repository_url)
        else:
            _repository_url = self._generate_full_url(url)
            _repository_path = self._generate_path(url)

        _existing_repository_path = self._check_path(_repository_path)

        if self.debug:
            print("DEBUG: _repository_url is " + _repository_url)
            print("DEBUG: _repository_path is " + _repository_path)
            print(
                "DEBUG: _existing_repository_path is " + str(_existing_repository_path)
            )

        if _existing_repository_path:
            self.repository = Repository(_existing_repository_path)
        else:
            shutil.rmtree(_repository_path, ignore_errors=True)
            self.repository = clone_repository(_repository_url, _repository_path)

    def ls_remotes(self):
        """
        TODO
        """

        if self.debug:
            print(
                "DEBUG: self.repository.remotes[0].name is "
                + self.repository.remotes[0].name
            )

        self.repository.remotes[0].ls_remotes()
