"""locust-plugin-git"""
import unittest
import shutil
from locust.test.testcases import LocustTestCase
from locust_plugin_git import GitUser


class TestGitUser(LocustTestCase):
    """
    TODO
    """

    def setUp(self):
        """
        TODO
        """

        super().setUp()

        _transport_protocol = "https"
        _debug = True

        class PyGitUser(GitUser):
            """
            TODO
            """

            host = "github.com"
            transport_protocol = _transport_protocol
            repository_url = "libgit2/pygit2"
            debug = _debug

        class LocustPluginGitUser(GitUser):
            """
            TODO
            """

            host = "gitlab.com"
            transport_protocol = _transport_protocol
            repository_url = "infinite-omicron/eng/locust-plugin-git"
            debug = _debug

        self.pygituser = PyGitUser(self.environment)
        self.locust_plugin_git_user = LocustPluginGitUser(self.environment)

    def tearDown(self):
        """
        TODO
        """

        super().tearDown()
        shutil.rmtree("pygit2")
        shutil.rmtree("locust-plugin-git")

    def test_github_repository_url(self):
        """
        TODO
        """

        self.assertEqual(
            # pylint: disable=protected-access
            self.pygituser.client._generate_full_url(self.pygituser.repository_url),
            "https://github.com/libgit2/pygit2",
        )

    def test_github_repository_path(self):
        """
        TODO
        """

        self.assertEqual(
            # pylint: disable=protected-access
            self.pygituser.client._generate_path(self.pygituser.repository_url),
            "pygit2",
        )

    def test_gitlab_repository_url(self):
        """
        TODO
        """

        self.assertEqual(
            # pylint: disable=protected-access
            self.locust_plugin_git_user.client._generate_full_url(
                self.locust_plugin_git_user.repository_url
            ),
            "https://gitlab.com/infinite-omicron/eng/locust-plugin-git",
        )

    def test_gitlab_repository_path(self):
        """
        TODO
        """

        self.assertEqual(
            # pylint: disable=protected-access
            self.locust_plugin_git_user.client._generate_path(
                self.locust_plugin_git_user.repository_url
            ),
            "locust-plugin-git",
        )


if __name__ == "__main__":
    unittest.main()
